# TODO: Reduce the number of hosts possible for the VPC
module "bci_vms" {
  source = "./modules/bci-vms"

  number_of_vms = var.number_of_vms
  vm_flavor     = var.vm_flavor
  vm_image_id   = var.vm_image_id
  vpc_id        = module.vpc.vpc_id
  subnet_id     = module.vpc.public_subnets[0]
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "bci"
  cidr = "10.0.0.0/16"

  azs             = ["eu-central-1a"]
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.101.0/24"]

  enable_nat_gateway = true

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
