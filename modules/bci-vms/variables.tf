variable "number_of_vms" {
  type        = number
  description = "Number of VMs to instantiate"
  default     = 2

  validation {
    condition     = var.number_of_vms >= 2
    error_message = "The number of VMs cannot be lower than 2"
  }

  validation {
    condition     = var.number_of_vms < 100
    error_message = "The number of VMs cannot be higher than 2"
  }
}

variable "vm_flavor" {
  type        = string
  description = "AWS EC2 instance type"
}

# TODO: Use by name
variable "vm_image_id" {
  type = string

  default = "ami-0c0d3776ef525d5dd"
}

variable "vpc_id" {
  type = string

  description = "VPC ID that the AWS EC2 instances should be part of"
}

variable "subnet_id" {
  type = string

  description = "Subnet ID that the AWS EC2 instances should be part of"
}
