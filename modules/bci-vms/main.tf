resource "aws_ec2_serial_console_access" "main" {
  enabled = true
}

data "remote_file" "ping_host" {
  count = var.number_of_vms

  conn {
    host = aws_instance.main[count.index].public_ip
    user = "terraform"
    # TODO: Do not hardcode use generated password
    password = "terraform"
  }

  path = "/home/terraform/ping_result.txt"
}

resource "null_resource" "ping_host" {
  count = var.number_of_vms

  connection {
    type = "ssh"
    user = "terraform"
    # TODO: Do not hardcode use generated password
    password = "terraform"
    host     = aws_instance.main[count.index].public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "ping -c 5 ${count.index == length(aws_instance.main) - 1 ? aws_instance.main[0].private_ip : aws_instance.main[count.index + 1].private_ip} > ping_result.txt"
    ]
  }
}


resource "aws_security_group" "ping_access" {
  name_prefix = "ping-access"
  description = "Allow Ping traffic"
  vpc_id      = var.vpc_id

  ingress {
    cidr_blocks = [
      data.aws_vpc.selected.cidr_block
    ]
    description = ""
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
  }

  egress {
    cidr_blocks = [
      data.aws_vpc.selected.cidr_block
    ]
    description = ""
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
  }
}

resource "aws_security_group" "ssh_access" {
  name_prefix = "ssh-access"
  description = "Allow SSH traffic"
  vpc_id      = var.vpc_id

  ingress {
    description = "SSH access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}

resource "aws_instance" "main" {
  count         = var.number_of_vms
  ami           = var.vm_image_id
  instance_type = var.vm_flavor

  vpc_security_group_ids = [aws_security_group.ssh_access.id, aws_security_group.ping_access.id]
  subnet_id              = var.subnet_id

  user_data_replace_on_change = true
  user_data = base64encode(<<EOF
#cloud-config

password: password1
ssh_pwauth: true

users:
  - name: admin
    primary_group: admin
    groups: sudo
    lock_passwd: false
    sudo: "ALL=(ALL) NOPASSWD:ALL"

  - name: terraform
    primary_group: terraform
    groups: sudo
    lock_passwd: false
    passwd: $6$E3XIiOunKT.a8pOZ$6Sn34i4y8OHv3Grb.TVRXwKWPHDtwL3G4bkJtgkOSInUxS7Sgpwb5DNOLF5aGocykKwWlnYdEcnwgVErzQg9V0
    sudo: "ALL=(ALL) NOPASSWD:ALL"

# Assign a random password to admin
chpasswd:
  expire: false
  list: |
    admin:RANDOM
EOF

  )
}

# resource "aws_autoscaling_group" "main" {
#   desired_capacity = var.number_of_vms
#   max_size         = var.number_of_vms
#   min_size         = var.number_of_vms

#   vpc_zone_identifier = var.subnet_ids

#   launch_template {
#     id      = aws_launch_template.main.id
#     version = aws_launch_template.main.latest_version
#   }

#   tag {
#     key                 = "Key"
#     value               = "Value"
#     propagate_at_launch = true
#   }

#   instance_refresh {
#     strategy = "Rolling"
#     preferences {
#       min_healthy_percentage = 50
#     }
#     triggers = ["tag"]
#   }
# }

# resource "aws_launch_template" "main" {
#   name_prefix = "bci-vms"
#   # image_id      = data.aws_ami.main.id
#   image_id      = var.vm_image_id
#   instance_type = var.vm_flavor

#   user_data = filebase64("${path.module}/cloud-init.yaml")
# }

# TODO: Query properly
# data "aws_ami" "main" {
#   most_recent = true

#   filter {
#     name   = "name"
#     values = [var.vm_image]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }
# }
