# BCI Ops solution

## Requirements

- Terraform (>= 1.3) installed
- AWS CLI v2 installed
- AWS credentials configured

## Usage

- Apply the changes to deploy infrastructure: `terraform apply -auto-approve`
- Get the `ping` results: `terraform output ping_results`
